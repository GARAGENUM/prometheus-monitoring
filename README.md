# PROMETHEUS & GRAFANA :bar_chart:

Ce projet vise à monitorer des serveurs via prometheus pour centraliser le scraping, node exporter pour les host metrics, cadvisor pour les metrics des conteneurs docker et grafana pour afficher les metrics sous forme de dashboards

![PROM](docs/prom.png)

## PREREQUIS :memo:

- [docker + compose plugin](https://git.legaragenumerique.fr/GARAGENUM/docker-install) :whale:

## CONFIGURATION :wrench:

### ALERTES VIA EMAIL :email:

Les alertes peuvent être configurées de deux manières:
- via Alertmanager (conventionnel)
- via Grafana (pour petites infras)

#### VIA ALERTMANAGER

- Configuration de la boîte mail pour les alertes:
```bash
nano alertmanager/alertmanager.yml
```

- Configuration des alertes:
```bash
nano alertmanager/alert.rules
```

> [doc](https://prometheus.io/docs/alerting/latest/alertmanager/)

#### VIA GRAFANA

- Provisionning du notifier (email) par default:
```bash
nano grafana/provisioning/notifiers.yml
```

- Configuration de la boîte mail pour les alertes:
```bash
nano grafana/config/grafana.ini
```

> [doc](https://grafana.com/docs/grafana/latest/alerting/fundamentals/notification-policies/)

### GRAFANA SSO KEYCLOAK :key:

- Créer un client sur keycloak en confidential pour obtenir le client-secret
- Entrer le nom de domaine de votre instance grafana
- Editer le fichier grafana/grafana.ini:
```ini
[server]
# The http port  to use
http_port = 3000
# The public facing domain name used to access grafana from a browser
domain = grafana.votre-domaine.tld

...

# pour rediriger auto vers SSO provider
[auth]
oauth_auto_login = true

...

[auth.generic_oauth]
enabled = true
scopes = openid email profile
name = Oauth
tls_skip_verify_insecure = true
allow_sign_up = true
client_id = grafana
client_secret = <client-secret-de-keycloak>
auth_url = https://votre-keycloak/auth/realms/votre-royaume/protocol/openid-connect/auth
token_url = https://votre-keycloak/auth/realms/votre-royaume/protocol/openid-connect/token
api_url = https://votre-keycloak/auth/realms/votre-royaume/protocol/openid-connect/userinfo

# Pour ne laisser que l'authentification keycloak
disable_login_form = true
```

### GRAFANA DASHBOARDS

- Configurer une home page avec des liens vers les dashboards:
```ini
default_home_dashboard_path = /etc/grafana/provisioning/dashboards/home.json
```

- Configuration du html dans le home.json:
```html
<!-- logo -->
<div style=\"text-align: center;\">\n\n  <img src=\"https://odoo.legaragenumerique.fr/web/image/website/2/logo/Le%20Garage%20Num%C3%A9rique?unique=0c6bb2e\" alt=\"Le Garage Numérique Logo\" />\n\n
<!-- liste des dashboards -->
<ul style=\"list-style-type: none; padding: 0;\">\n    
    <li><a href=\"https://monitoring.legaragenumerique.fr/d/6DAk-0kIk/node-exporter-garage-stack\">SERVERS METRICS</a></li><br>\n    
    <li><a href=\"https://monitoring.legaragenumerique.fr/d/V8Sbl30Ik/garage-docker-stack\">DOCKER DASHBOARD</a></li><br>\n    
    <li><a href=\"https://monitoring.legaragenumerique.fr/d/7ZpFYG2Iz/ssh-dashboard\">SSH LOGS</a></li><br>\n    
    <li><a href=\"https://monitoring.legaragenumerique.fr/d/EbtmMBZSk/serveurs-alertes\">ALERTES METRICS</a></li></br>\n    
    <li><a href=\"https://monitoring.legaragenumerique.fr/d/d2d27b91-1da3-4098-b9f2-8877b51756dkd/geoip-fail2ban\">GEO FAIL2BAN</a></li>\n  </ul>\n\n</div>      "
```

> Pour chaque dashboard créées, récupérer le lien dans l'url pour l'ajouter à la liste.

## UTILISATION :rocket:

- Démarrer la stack:
```bash
docker compose up -d
```

> Grafana est accessible via l'adresse: http://<IP-SERVER>:3000

### NODE EXPORTER TLS :link:

Pour déployer un node exporter sur un serveur distant:
> voir branche [node-exporter](https://git.legaragenumerique.fr/GARAGENUM/prometheus-monitoring/src/branch/node-exporter)


## TO DO :bookmark_tabs:

- [x] node exporter
- [x] node exporter -> prometheus via https
- [x] alert manager config / grafana alert via mail config
- [x] provision dashboard / notifier par default
- [ ] dashboard for Grafana amd64:
    - [x] host metrics
    - [ ] cadvisor for docker + dashboard
- [ ] config alertes sous grafana + images
